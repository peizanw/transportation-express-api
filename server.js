/** 
 * Example of RESTful API using Express and NodeJS
 * @author Clark Jeria
 * @version 0.0.2
 */

var express    = require('express');
var app        = express();
var bodyParser = require('body-parser');
var cookieParser = require('cookie-parser');
var express_jwt = require('express-jwt');

app.use(cookieParser());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

var port = process.env.PORT || 8080;

var mongoose    = require('mongoose');
mongoose.connect('mongodb://app_user:password@ds035826.mlab.com:35826/cmu_sv_app');

var Timing = require('./app/util/timing');

var session = require('./routes/session');
var router = require('./routes/router');
var cars = require('./routes/cars');
var drivers = require('./routes/drivers');
var passengers = require('./routes/passengers');
var paymentAccounts = require('./routes/paymentaccounts');
var rides = require('./routes/rides');

var key = session.key;


/**
 * Business logic middleware
 */
app.use('/api/session', session.router);
app.use('/api', cars);
app.use('/api', drivers);
app.use('/api', passengers);
app.use('/api', paymentAccounts);
app.use('/api', rides);
app.use('/api', router);

app.use(function(err, req, res, next) {
	if (err.name === 'UnauthorizedError') {
		res.status(401).json({
			"statusCode": "404",
			"errorCode": "1000",
			"errorMessage": "Unauthorized"	
		});
	}
});

app.use(function(req, res, next) {
  res.status(404).json({"errorCode": "1012", "errorMessage" : "Invalid Resource Name", "statusCode" : "404"});  
});

app.listen(port);
console.log('Service running on port ' + port);

module.exports = app;
