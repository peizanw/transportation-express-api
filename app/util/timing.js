/**
 * timing.js
 *  
 * Created by Daniel Wang
 */

function Timing() {

	this.startTime = new Date();

	this.click = function (str) {
		var currentTime = new Date();
		var timediff = currentTime.getTime() 
			- this.startTime.getTime();
		timediff = "" + timediff + " ms";

		console.log(str, timediff);
	}
}


module.exports = Timing;
