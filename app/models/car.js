/** 
 * Mongoose Schema for the Entity Car
 * @author Clark Jeria
 * @version 0.0.3
 */

var mongoose     = require('mongoose');
var Schema       = mongoose.Schema;

var CarSchema   = new Schema({
	make: {
		type: String, 
		maxlength: 18,
		required: true
	},
    model: {
		type: String, 
		maxlength: 18,
		required: true
	},
    license: {
		type: String, 
		maxlength: 10,
		required: true
	},
    doorCount: {
		type: Number, 
		min: 1,
		max: 8,
		required: true
	},
    driver: { 
		type: Schema.Types.ObjectId, 
		ref: 'Driver' 
	}
});

module.exports = mongoose.model('Car', CarSchema);
