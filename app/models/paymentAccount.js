/** 
 * Mongoose Schema for the Entity PaymentAccount
 * @author Clark Jeria
 * @version 0.0.1
 */

var mongoose     = require('mongoose');
var Schema       = mongoose.Schema;

var PaymentAccountSchema   = new Schema({
    accountType: {
		type: String,
		maxlength: 18
	},
    accountNumber: Number,
    expirationDate: Number,
    nameOnAccount: {
		type: String,
		maxlength: 18
	}, 
    bank: {
		type: String,
		maxlength: 18
	}
});

module.exports = mongoose.model('PaymentAccount', PaymentAccountSchema);
