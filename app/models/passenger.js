/** 
 * Mongoose Schema for the Entity Passenger
 * @author Clark Jeria
 * @version 0.0.3
 */

var mongoose     = require('mongoose');
var Schema       = mongoose.Schema;

var PassengerSchema   = new Schema({
    firstName: {
		type: String,
		minlength: 1,
		maxlength: 15
	},
    lastName: {
		type: String,
		minlength: 1,
		maxlength: 15
	},
    emailAddress: {
		type: String,
		match: /[a-zA-Z0-9_.]+\@[a-zA-Z](([a-zA-Z0-9-]+).)*/
	},
    password: String,
    addressLine1: {
		type: String,
		maxlength: 50
	},
    addressLine2: {
		type: String,
		maxlength: 50
	},
    city: {
		type: String,
		maxlength: 50
	},
    state: {
		type: String,
		maxlength: 2
	},
    zip: {
		type: String,
		maxlength: 5 
	},
    phoneNumber: {
		type: String,
		match: /[0-9]{3}-[0-9]{3}-[0-9]{4}/
	}	
});

module.exports = mongoose.model('Passenger', PassengerSchema);
