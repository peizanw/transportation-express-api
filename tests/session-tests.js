var supertest = require('supertest');
var assert = require('assert');
var app = require('../server.js');

exports.could_not_access_without_session = function(done) {
	supertest(app)
		.get('/api/cars')
		.expect(401)
		.end(function(err, res) {
			if (err) {
				console.log(err);
			}
			done();
		});
}

var user = {
	username: 'test-user',
	password: '123455'
};

exports.should_signup = function(done) {
	supertest(app)
		.post('/api/session/signup')
		.send(user)
		.expect(201)
		.end(function(err, res) {
			if (err) {
				console.log(err);
			}
			
			assert.ok(res.body.username === user.username);
			done();
		});
}

var token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6IndhbmciLCJpYXQiOjE0NzYzMzM4Nzl9.yn5cwmL5EHIP8VI9jgAMxEjjnwcYcxROm3iCpZgIP2Y";

exports.could_access_after_signup = function(done) {
	supertest(app)
		.get('/api/cars')
		.set('Cookie', ['token=' + token])
		.expect(200)
		.end(function(err, res) {
			if (err) {
				console.log(err);
			}
			
			done();
		});
}
