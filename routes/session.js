var express = require('express');
var router = express.Router();
var jwt = require('jsonwebtoken');

var users = {};
var key = "XwGKGk5K19dTpo72E000";

/**
 * User object used in signup/login
 */
function User() {
	this.username = "";
	this.pwd = "";

	this.getAuthObject = function() {
		return {username: this.username};
	}
}

router.route('/signup')
/**
 * POST a User object to signup 
 * return 201 if the user is created successfully
 * 400 if there is an error
 */
.post(function(req, res) {
	var name = req.body.username;
	var pwd = req.body.password;

	if (!!name && !!pwd){
		var newUser = new User();
		newUser.username = name;
		newUser.pwd = pwd;
		users[name] = pwd;

		var myToken = jwt.sign(newUser.getAuthObject(), key);
		res.cookie("token", myToken);

		res.status(201).json(newUser.getAuthObject());
	}else {
		res.status(400).end('Please provide username and pwd');
	}
});

router.route('/login')
/**
 * POST a User object to login with
 * return 200 on success
 * return 400 if there is an error
 */
.post(function(req, res) {
	var name = req.body.username;
	var pwd = req.body.password;

	if (!!name && !!pwd && pwd == users[name]){
		var newUser = new User();
		newUser.username = name;
		newUser.pwd = pwd;

		var myToken = jwt.sign(newUser.getAuthObject(), key);
		res.cookie("token", myToken);

		res.status(200).json(newUser.getAuthObject());
	}else {
		res.status(400).end('Please provide username and pwd');
	}
});


module.exports.router = router;
module.exports.key = key;
