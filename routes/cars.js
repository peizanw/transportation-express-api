/** 
 * Express Route: /cars
 * @author Clark Jeria
 * @version 0.0.3
 */
var express = require('express');
var router = express.Router();
var util = require('util');
var mongoose     = require('mongoose');
var express_jwt = require('express-jwt');

var Car = require('../app/models/car');
var Timing = require('../app/util/timing');
var key = require('./session').key;

router
	.all('/cars*', express_jwt({
		secret: key,
		getToken: function(req) {
			return req.cookies.token;
		}
	}));

router.route('/cars') 
    /**
     * GET call for the car entity (multiple).
     * @returns {object} A list of cars. (200 Status Code)
     * @throws Mongoose Database Error (500 Status Code)
     */
    .get(function(req, res){
		var timing = new Timing();

        Car.find(function(err, cars){
            if(err){
                res.status(400).json({
                    "statusCode": "400",
                    "errorCode": "1002",
                    "errorMessage": "Can not find any cars"
                });
            }else{
				timing.click("DB query cars");
                res.json(cars);
            }
        });
    })

    /**
     * POST call for the car entity.
     * @param {string} license - The license plate of the new car
     * @param {integer} doorCount - The amount of doors of the new car
     * @param {string} make - The make of the new car
     * @param {string} model - The model of the new car
     * @returns {object} A message and the car created. (201 Status Code)
     * @throws Mongoose Database Error (500 Status Code)
     */
    .post(function(req, res){
        if (typeof req.body.make === "undefined" 
			|| req.body.make.length > 18) {
            res.sendStatus(400);
            return;

        }

        var car = new Car();
        car.license = req.body.license;
        car.doorCount = req.body.doorCount;
        car.make = req.body.make;
        car.model = req.body.model;

        car.save(function(err){
            if(err){
                res.status(500).json({
                    "statusCode": "500",
                    "errorCode": "1004",
                    "errorMessage": err
                });
            }else{
                res.status(201).json(car);
            }
        });
    });

/** 
 * Express Route: /cars/:car_id
 * @param {string} car_id - Id Hash of Car Object
 */
router.route('/cars/:car_id')
    /**
     * GET call for the car entity (single).
     * @returns {object} the car with Id car_id. (200 Status Code)
     * @throws Mongoose Database Error (500 Status Code)
     */
    .get(function(req, res){
        /**
         * Add extra error handling rules here
         */
        if (!mongoose.Types.ObjectId.isValid(req.params.car_id)) {
            res.status(404).json({
                "statusCode": "404",
                "errorCode": "1002",
                "errorMessage": "Car doesn't exist"
            });
            return;
        }

        Car.findById(req.params.car_id, function(err, car){
            if(err){
                res.status(500).send(err);
            }else{
                if (!car)
                    res.status(404).json({
						"statusCode": "404",
						"errorCode": "1002",
						"errorMessage": "Car doesn't exist"	
					});
                else
                    res.json(car);
            }
        });  
    })
    /**
     * PATCH call for the car entity (single).
     * @param {string} license - The license plate of the new car
     * @param {integer} doorCount - The amount of doors of the new car
     * @param {string} make - The make of the new car
     * @param {string} model - The model of the new car
     * @returns {object} A message and the car updated. (200 Status Code)
     * @throws Mongoose Database Error (500 Status Code)
     */
    .patch(function(req, res){
        /**
         * Add extra error handling rules here
         */

        Car.findById(req.params.car_id, function(err, car){
            if(err){
                res.status(400).send(err);
            }else{
				var i;
				var keys = Object.keys(req.body);

				for (i = 0; i < keys.length; i++) {
					if (car[keys[i]] && keys[i] != '_id') {
						car[keys[i]] = req.body[keys[i]];
					}else {
						res.status(400).json({
							"statusCode": "400",
							"errorCode": "1003",
							"errorMessage": "Invalid property: " + keys[i]
						});

						return;
					}
				}

                car.save(function(err){
                    if(err){
                        res.status(400).send(err);
                    }else{
                        res.json(car);
                    }
                });
            }
        });
    })
    /**
     * DELETE call for the car entity (single).
     * @returns {object} A string message. (200 Status Code)
     * @throws Mongoose Database Error (500 Status Code)
     */
    .delete(function(req, res){
        /**
         * Add extra error handling rules here
         */
        Car.remove({
            _id : req.params.car_id
        }, function(err, car){
            if(err){
                res.status(400).json({
					"statusCode": "400",
					"errorCode": "1002",
					"errorMessage": "Car doesn't exist"	
				});
            }else{
                res.json({"message" : "Car Deleted"});
            }
        });
    });

module.exports = router;
