/** 
 * Express Route: /rides
 * @author Clark Jeria
 * @version 0.0.3
 */
var express = require('express');
var router = express.Router();
var util = require('util');
var mongoose = require('mongoose');

var Ride = require('../app/models/ride');

/**
 * Here you must add the routes for the Ride entity
 * /rides/:id/routePoints (POST)
 * /rides/:id/routePoints (GET)
 * /rides/:id/routePoint/current (GET)
 */

		
router.route('/rides')
	/**
	 * POST a new ride
	 * return 201 on created
	 */
	.post(function(req, res) {
		var ride = new Ride();

		ride.rideType = req.body.rideType;
		ride.status = "REQUESTED";
		ride.requestTime = (new Date()).getTime();
		
		ride.save(function(err) {
			if (!!err) {
				res.status(500).send(err);
			}else {
				res.status(201).json(ride);
			}
		});
	})
	/**
	 * GET all the rides
	 * return a list of all rides on success
	 */
	.get(function(req, res) {
		Ride.find(function(err, rides) {
			if (!!err) {
				res.status(500).send(err);
			}else {
				res.json(rides);
			}
		});
	});

router.route('/rides/:id/routePoints')
	/**
	 * GET all the route points for a specific ride
	 * return a list of points on success
	 */
	.get(function(req, res) {
		if (!mongoose.Types.ObjectId.isValid(req.params.id)) {
			var err = {
				errorCode: 4000,
				message: 'Ride not found'
			};
			res.status(404).json(err);
			return;
		}else {

			Ride.findById(req.params.id, function(err, ride) {
				if (!!err) {
					res.status(500).send(err);
				}else {
					if (!ride) {
						var err = {
							errorCode: 4000,
							message: 'Ride not found'
						};
						res.status(404).json(err);
					}else {
						res.json(ride.route);
					}
				}
			});

		}
	})
	/**
	 * POST a new route point to a route
	 * return 201 on created
	 */
	.post(function(req, res) {
		if (!mongoose.Types.ObjectId.isValid(req.params.id)) {
			var err = {
				errorCode: 4000,
				message: 'Ride not found'
			};
			res.status(404).json(err);
			return;
		}

		Ride.findById(req.params.id, function(err, ride) {
			if (!!err) {
				res.status(500).send(err);
				return;
			}
			if (!ride) {
				var err = {
					errorCode: 4000,
					message: 'Ride not found'
				};

				res.status(404).json(err);
				return;
			}
			// append new point to points
			var new_point = req.body;
			ride.route.push(new_point);

			// save ride
			ride.save(function(err) {
				if (!!err) {
					res.status(500).send(err);
				}else {
					res.status(201).json(ride);
				}
			});	
		});
		
	});

router.route('/rides/:id/routePoints/current')
	/**
	 * GET the latest point of a ride
	 */
	.get(function(req, res) {
		if (!mongoose.Types.ObjectId.isValid(req.params.id)) {
			var err = {
				errorCode: 4000,
				message: 'Ride not found'
			};
			res.status(404).json(err);
			return;
		}

		Ride.findById(req.params.id, function(err, ride) {
			if (!!err) {
				res.status(500).send(err);
				return;
			}
			if (!ride) {
				var err = {
					errorCode: 4000,
					message: 'Ride not found'
				};
				res.status(404).json(err);
				return;
			}
			if (ride.route.length > 0) {
				res.json(ride.route[ride.route.length - 1]);
			}else {
				res.json({});
			}
		});
	});


module.exports = router;

